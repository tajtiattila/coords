// Copyright 2014 Attila Tajti. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"os"
	"regexp"
	"runtime"
	"sync"
	"time"
)

var (
	distlimit float64
	nthreads  int

	ndecdigits int = 3

	// set this to a bit higher to check for correctness, in this case
	// also distances off ±0.001 should be displayed
	prec    float64 = 0.0005
	precmul float64 = 1e3
)

func main() {
	start := time.Now()

	var (
		factor  float64
		pat     string
		verbose bool
		maxlist int
	)
	flag.Float64Var(&distlimit, "l", 200, `l is the distance in Ly under which an entry is processed.
	Memory use is proportional to the square of l.`)
	flag.IntVar(&nthreads, "j", 4, `j is the number of parallel jobs (threads) to use, it should be
	set to the number of logical processors on the system to improve performance.
	Memory use is proportional to j.`)
	flag.IntVar(&ndecdigits, "d", 3, `decimal digits to use from distance values`)
	flag.Float64Var(&factor, "f", 1.0, `precision adjust factor: multiply the default the precison
	of (1/10^d)/2 with this value`)
	flag.StringVar(&pat, "s", ".*", `process systems matching this regex`)
	flag.BoolVar(&verbose, "v", false, `verbose output`)
	flag.IntVar(&maxlist, "n", 10, `show coords when there are at most this many candidates`)
	flag.BoolVar(&gridpbase32, "b32", false, `use base32 for coords display`)
	flag.Parse()

	runtime.GOMAXPROCS(nthreads)

	rexp, err := regexp.Compile(pat)
	verify(err)

	v := int64(1)
	for i := 0; i < ndecdigits; i++ {
		v *= 10
	}
	precmul = float64(v)
	prec = (1 / precmul) / 2 * factor

	var fn string
	if flag.NArg() == 0 {
		fn = "systems.json"
	} else {
		fn = flag.Arg(0)
	}
	data, err := edsysdata(fn)
	verify(err)

	for i := range data.vsys {
		sys := &data.vsys[i]
		for j := range sys.Distances {
			de := &sys.Distances[j]
			de.xdist = math.Floor(de.Distance*precmul+0.5) / precmul
		}
	}

	fmt.Println("#", len(data.vsys), "systems total")
	fmt.Printf("# using %d decimal digits and maximum error of %f\n", ndecdigits, prec)

	if gridpbase32 {
		fmt.Println("# showing coords in format: ±whole·frac (frac is a base32 digit)")
	}

	m := make(map[string]Sys)
	for _, e := range data.vsys {
		if !e.Calculated {
			m[e.Name] = e
		}
	}

	/*
		// Note: the code in this comment block could be used
		// as a single-threaded solution for the rest of main()
		// -- in case the occasional reader does not know go
		for _, e := range data.vsys {
			if !e.Calculated {
				continue
			}
			if rexp.MatchString(e.Name) {
				fmt.Println(job(m, e))
			}
		}
	*/
	var wg sync.WaitGroup
	chw, chres := make(chan *Sys), make(chan result)
	for i := 0; i < nthreads; i++ {
		wg.Add(1)
		go func() {
			for e := range chw {
				chres <- job(m, e)
			}
			wg.Done()
		}()
	}
	go func() {
		wg.Wait()
		close(chres)
	}()
	go func() {
		for i := range data.vsys {
			e := &data.vsys[i]
			if !e.Calculated {
				continue
			}
			if rexp.MatchString(e.Name) {
				chw <- e
			}
		}
		close(chw)
	}()

	var warn []result
	for res := range chres {
		if !res.prt(data, verbose, maxlist) {
			warn = append(warn, res)
		}
	}
	if len(warn) != 0 {
		fmt.Println("\n# warnings:")
		for _, res := range warn {
			res.prt(data, verbose, maxlist)
		}
	}
	fmt.Println("# completed in", time.Now().Sub(start))
}

// job checks the validity of a Sys entry, and returns its
// findings as a result using Calc().
func job(m map[string]Sys, e *Sys) result {
	mindist := 1e9
	for _, de := range e.Distances {
		if de.xdist < mindist {
			mindist = de.xdist
		}
	}
	if distlimit < mindist {
		return &skipresult{e, fmt.Sprintf("minimal distance %.*f above limit", ndecdigits, mindist)}
	}
	var vnear []Dist
	for _, de := range e.Distances {
		if de.xdist < distlimit {
			vnear = append(vnear, de)
		}
	}
	if len(vnear) == 0 {
		return &skipresult{e, "no verified bases"}
	}
	vp, _ := Calc(vnear, e.Distances)
	return &calcresult{e, vp}
}

type Data struct {
	vsys []Sys
	ref  map[string]*Sys
}

type Sys struct {
	Name        string
	X, Y, Z     *float64
	Contributor string
	Calculated  bool
	Distances   []Dist

	hasp bool
	P    Gridp
}

type Dist struct {
	System   string
	Distance float64

	xdist    float64 // precision adjusted distance
	refp     Gridp   // reference system position
	mi2, ma2 int64   // minimum and maximum possible integer squared distance
}

// edsysdata loads files of system.json format
func edsysdata(fn string) (Data, error) {
	f, err := os.Open(fn)
	if err != nil {
		return Data{}, err
	}
	defer f.Close()

	data := Data{ref: make(map[string]*Sys)}
	err = json.NewDecoder(f).Decode(&data.vsys)
	if err != nil {
		return Data{}, err
	}
	for i := range data.vsys {
		e := &data.vsys[i]
		if e.X == nil || e.Y == nil || e.Z == nil {
			e.hasp = false
			if !e.Calculated {
				fmt.Printf("! %s not marked as calculated, but has no position\n", e.Name)
			}
			e.Calculated = true
		} else {
			e.hasp = true
			c := []float64{*e.X, *e.Y, *e.Z}
			for j := 0; j < 3; j++ {
				ic := Gridc(c[j] * 32)
				if !e.Calculated && float64(ic) != c[j]*32 {
					cname := []string{"X", "Y", "Z"}
					fmt.Printf("! %s position dropped, has off-grid %s coordinate %f", e.Name, cname[j], c[j])
					e.hasp = false
				}
				e.P[j] = ic
			}
		}
		for j := range e.Distances {
			de := &e.Distances[j]
			de.mi2, de.ma2 = IntDistRng(de.Distance)
		}
		if e.hasp {
			data.ref[e.Name] = e
		}
	}
	for i := range data.vsys {
		e := &data.vsys[i]
		if !e.Calculated {
			continue
		}
		var xde []Dist
		for _, de := range e.Distances {
			if sys, ok := data.ref[de.System]; ok {
				de.refp = sys.P
				xde = append(xde, de)
			} else {
				fmt.Printf("! invalid reference '%s' for %s dropped\n", de.System, e.Name)
			}
		}
		e.Distances = xde
	}
	return data, nil
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
