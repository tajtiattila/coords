// Copyright 2014 Attila Tajti. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"runtime"
)

// Gridc represents an integer grid coordinate in 1/32 Ly units
type Gridc int32

// Gridp is an position on the 3D integer grid using 1/32 Ly units
type Gridp [3]Gridc

func (p Gridp) Sub(q Gridp) Gridp { return Gridp{p[0] - q[0], p[1] - q[1], p[2] - q[2]} }

func (p Gridp) Abs2() int64 {
	x, y, z := int64(p[0]), int64(p[1]), int64(p[2])
	return x*x + y*y + z*z
}

var (
	gridpbase32 bool
)

const (
	base32 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
)

func (p Gridp) String() string {
	if gridpbase32 {
		return fmt.Sprintf("%s,%s,%s", gridcb32(p[0]), gridcb32(p[1]), gridcb32(p[2]))
	}
	return fmt.Sprintf("%.5f,%.5f,%.5f", float64(p[0])/32, float64(p[1])/32, float64(p[2])/32)
}

func gridcb32(c Gridc) string {
	var s string
	if c < 0 {
		s, c = "-", -c
	}
	return s + fmt.Sprintf("%d·%c", c/32, base32[c%32])
}

// Calc generates possible coordinates based on the distance input.
// Value is within 0..1 and reports how good the coordinates are (1=exact match)
func Calc(v []Dist, vcmp []Dist) (positions []Gridp, value float64) {
	bmap := make(map[Gridp]bool)
	for i, e := range v {
		// create map for the reference system
		DistGridp(e.xdist, func(c Gridp) {
			perms(c, func(p Gridp) {
				p[0] += e.refp[0]
				p[1] += e.refp[1]
				p[2] += e.refp[2]
				for j, f := range vcmp {
					if i != j {
						dx := int64(p[0] - f.refp[0])
						dy := int64(p[1] - f.refp[1])
						dz := int64(p[2] - f.refp[2])
						d2 := dx*dx + dy*dy + dz*dz
						if f.mi2 <= d2 && d2 <= f.ma2 {
							bmap[p] = true
						}
					}
				}
			})
		})
	}
	posm := make(map[Gridp]int16)
	var bestok int16
	for p := range bmap {
		nok := int16(0)
		for _, e := range vcmp {
			dx := int64(p[0] - e.refp[0])
			dy := int64(p[1] - e.refp[1])
			dz := int64(p[2] - e.refp[2])
			d2 := dx*dx + dy*dy + dz*dz
			if e.mi2 <= d2 && d2 <= e.ma2 {
				nok++
			}
		}
		posm[p] = nok
		if nok > bestok {
			bestok = nok
		}
	}
	bmap = nil
	for p, nok := range posm {
		if nok == bestok {
			positions = append(positions, p)
		}
	}
	posm = nil
	runtime.GC()
	value = float64(bestok) / float64(len(vcmp))
	return
}

var allperms = []Gridp{{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}}

// Perms generates all distinct permutations of p,
// assuming p[0] >= p[1] >= p[2], and feeds them to f.
// It handles the case when some coordinates are equal.
func perms(p Gridp, f func(Gridp)) {
	switch {
	case p[0] == p[1]:
		if p[1] == p[2] {
			signs(p, f)
		} else {
			d, s := p[0], p[2]
			signs(Gridp{d, d, s}, f)
			signs(Gridp{d, s, d}, f)
			signs(Gridp{s, d, d}, f)
		}
	case p[1] == p[2]:
		s, d := p[0], p[2]
		signs(Gridp{d, d, s}, f)
		signs(Gridp{d, s, d}, f)
		signs(Gridp{s, d, d}, f)
	default:
		for _, q := range allperms {
			signs(Gridp{p[q[0]], p[q[1]], p[q[2]]}, f)
		}
	}
}

// perms generates all distinct signs variants of p,
// assuming all compontnts are positive, and feeds them to f.
// It handles the case when some coordinates are zero.
func signs(p Gridp, f func(Gridp)) {
	for m := uint(0); m < 8; m++ {
		d := p
		if (m & 1) != 0 {
			if d[0] == 0 {
				continue
			}
			d[0] = -d[0]
		}
		if (m & 2) != 0 {
			if d[1] == 0 {
				continue
			}
			d[1] = -d[1]
		}
		if (m & 4) != 0 {
			if d[2] == 0 {
				continue
			}
			d[2] = -d[2]
		}
		f(d)
	}
}

// DistGridp generates all valid coordinate triplets
// i,j,k such that sqrt(i²+j²+k²) yields a screen value
// dist, and i <= j <= k holds. The triplets are feeded to f.
func DistGridp(dist float64, f func(Gridp)) {
	mi, ma := IntDistRng(dist)
	bound := ceil(isqrt(ma))
	for i := bound / 2; i < bound; i++ {
		lmi, lma := mi-i*i, ma-i*i
		pairs(lmi, lma, func(j, k int64) {
			if i >= j {
				f(Gridp{Gridc(i), Gridc(j), Gridc(k)})
			}
		})
	}
}

// IntDistRng calculates the range of squared integer values whose
// square root is within the prec constant from dist, and
// returns the minimum and maximum for this value.
func IntDistRng(dist float64) (int64, int64) {
	sf := screenFloat(float32(dist))
	ni, na := float32(dist-prec), float32(dist+prec)
	mi, ma := ceil(ni*ni*1024), floor(na*na*1024)
	for sf == screenValue(sqrtf(float32(mi-1)/1024)) {
		mi--
	}
	for sf == screenValue(sqrtf(float32(ma+1)/1024)) {
		ma++
	}
	return mi, ma
}

// pairs calculates all coordinate pairs i, j such
// that their squared summ falls between s is lmi <= s <= lma,
// and i < j holds, and feed them to f.
func pairs(lmi, lma int64, f func(int64, int64)) {
	bound := floor(sqrtf(float32(lma) / 2))
	for i := bound; i <= lma; i++ {
		nmi, nma := lmi-i*i, lma-i*i
		if nmi < 0 {
			nmi = 0
		}
		if nma < 0 {
			return
		}
		jmi, jma := floor(isqrt(nmi)), ceil(isqrt(nma))
		if i < jma {
			jma = i
		}
		i2 := i * i
		for j := jmi; j <= jma; j++ {
			n := i2 + j*j
			if lmi <= n && n <= lma {
				f(i, j)
			}
		}
	}
}

type screenFloat int64

func screenValue(f float32) screenFloat {
	return screenFloat(floor(f*float32(precmul) + 0.5))
}

func isqrt(i int64) float32 { return sqrtf(float32(i)) }
func ceil(f float32) int64  { return int64(ceilf(f)) }
func floor(f float32) int64 { return int64(floorf(f)) }

func iabs2(p Gridp) int64 {
	return int64(p[0])*int64(p[0]) + int64(p[1])*int64(p[1]) + int64(p[2])*int64(p[2])
}
func abs(p Gridp) float32 { return sqrtf(float32(iabs2(p))) }
