package main

import (
	"fmt"
	"io"
	"os"
	"strings"
	"unicode/utf8"
)

type result interface {
	prt(data Data, verbose bool, nlimit int) (posok bool)
}

type skipresult struct {
	*Sys   // system being checked
	reason string
}

func (r *skipresult) prt(data Data, verbose bool, nlimit int) (posok bool) {
	fmt.Printf("# %s skipped (%s)\n", quote(r.Sys.Name), r.reason)
	return true
}

type calcresult struct {
	*Sys         // system being checked
	pv   []Gridp // possible coordinates
}

func (r *calcresult) prt(data Data, verbose bool, nlimit int) (posok bool) {
	if len(r.pv) == 0 {
		fmt.Printf("! %s has no possible coordinates\n", quote(r.Name))
		return false
	}
	if len(r.pv) > nlimit {
		fmt.Printf("! %s has too many, %d possible coordinates\n", quote(r.Name), nlimit)
		return false
	}
	a := azr(data, verbose, r.Sys)
	w := WordWrapper(os.Stdout, "     ")
	if len(r.pv) == 1 {
		cp := r.pv[0]
		if !r.hasp {
			// nothing to compare against
			fmt.Fprintf(w, "+ %s is at %s: ", quote(r.Sys.Name), cp)
			a.pos(w, cp)
			return false
		} else {
			if r.Sys.P == cp {
				fmt.Fprintf(w, "= %s is verified at %s: ", quote(r.Sys.Name), cp)
				a.pos(w, cp)
				return true
			} else {
				fmt.Fprintf(w, "! %s should be at %s: ", quote(r.Sys.Name), cp)
				a.pos(w, cp)
				fmt.Fprintf(w, "  and not %s: ", quote(r.Sys.Name), r.Sys.P)
				a.pos(w, r.Sys.P)
				return false
			}
		}
	}

	fmt.Printf("? %s has %d candidate positions\n", quote(r.Sys.Name), len(r.pv))
	showp := !r.hasp
	for _, p := range r.pv {
		var remark string
		if r.hasp && p == r.Sys.P {
			showp = false
			remark = " (json position)"
		}
		w := WordWrapper(os.Stdout, "     ")
		fmt.Fprintf(w, "  %s%s: ", p, remark)
		a.pos(w, p)
	}
	if showp {
		w := WordWrapper(os.Stdout, "     ")
		fmt.Fprintf(w, "! %s json pos %s invalid: ", quote(r.Sys.Name), r.Sys.P)
		a.pos(w, r.Sys.P)
	}
	return false
}

type analyzer struct {
	data    Data
	sys     *Sys
	verbose bool
}

func azr(data Data, verbose bool, sys *Sys) *analyzer {
	return &analyzer{data, sys, verbose}
}

func (a *analyzer) pos(w io.Writer, p Gridp) {
	analyzePosition(w, a.data, p, a.sys, a.verbose)
}

func analyzePosition(w io.Writer, data Data, p Gridp, sys *Sys, verbose bool) {
	var ntotal, nbase, ncalcd int // number of total, non-calcd and calcd refs
	var mtotal, mbase, mcalcd int // number of total, non-calcd and calcd refs matching
	var bm, cm []string           // excluded (non matching) base and calcd ref systems

	for _, refd := range sys.Distances {
		refsys := data.ref[refd.System]
		ntotal++
		if refsys.Calculated {
			ncalcd++
		} else {
			nbase++
		}
		d2 := refsys.P.Sub(p).Abs2()
		if refd.mi2 <= d2 && d2 <= refd.ma2 {
			mtotal++
			if refsys.Calculated {
				mcalcd++
			} else {
				mbase++
			}
		} else {
			if refsys.Calculated {
				cm = append(cm, quote(refsys.Name))
			} else {
				bm = append(bm, quote(refsys.Name))
			}
		}
	}
	if ntotal == 0 {
		panic("empty result")
	}
	if ntotal == mtotal {
		fmt.Fprint(w, "perfect match\n")
		return
	}
	fmt.Fprintf(w, "%.2f%% valid (%d/%d base, %d/%d calcd) match",
		float64(mtotal)/float64(ntotal)*100,
		mbase, nbase,
		mcalcd, ncalcd)
	if verbose && len(bm)+len(cm) != 0 {
		fmt.Fprint(w, ", with ")
		if len(bm) != 0 {
			fmt.Fprint(w, strings.Join(bm, ", "), " base")
		}
		if len(cm) != 0 {
			if len(bm) != 0 {
				fmt.Fprint(w, " and ")
			}
			fmt.Fprint(w, strings.Join(cm, ", "), " calcd")
		}
		fmt.Fprint(w, " excluded")
	}
	fmt.Fprintln(w)
	return
}

const (
	qleft  = '‘'
	qright = '’'
)

func quote(n string) string {
	return string(qleft) + n + string(qright)
}

func WordWrapper(w io.Writer, cpfx string) io.Writer {
	n := 0
	for _ = range cpfx {
		n++
	}
	return &wordwrapper{
		w:       w,
		cpfx:    cpfx,
		cpfxlen: n,
	}
}

type wordwrapper struct {
	w io.Writer

	cpfx      string // continuation prefix
	cpfxlen   int    // number of runes in cpfx
	llen      int    // number of runes in current line
	addprefix bool   // prefix needs to be added
	linechars bool   // line has any chars printed since beginning or after last enter
}

func (w *wordwrapper) Write(p []byte) (n int, err error) {
	s, nr, q := 0, 0, false
	var m, siz int
	for i := 0; i < len(p); i += siz {
		var ch rune
		ch, siz = utf8.DecodeRune(p[i:])
		switch ch {
		case ' ':
			if !q {
				m, err = w.word(nr, p[s:i])
				n += m
				if err != nil {
					return
				}
				s, nr = i, siz
			}
		case '\n':
			m, err = w.word(nr+1, p[s:i+siz])
			n += m
			if err != nil {
				return
			}
			s, nr, q = i+siz, 0, false
		case qleft:
			q = true
			nr++
		case qright:
			q = false
			nr++
		default:
			nr++
		}
	}
	m, err = w.word(nr, p[s:])
	n += m
	return
}

func (w *wordwrapper) word(nrunes int, p []byte) (n int, err error) {
	//fmt.Fprintf(os.Stderr, "word: %d/%d %#v llen=%d\n", nrunes, len(p), string(p), w.llen)
	//if len(p) == 0 || len(p) == 1 && p[0] == ' ' {
	if len(p) == 0 {
		return 0, nil
	}
	mrunes := nrunes
	if p[len(p)-1] == '\n' {
		mrunes--
	}
	if w.linechars && w.llen+mrunes >= 80 {
		n, err = w.w.Write([]byte{'\n'})
		if err != nil {
			return 0, err
		}
		w.addprefix = true
		w.llen = 0
	}
	if w.addprefix {
		n, err = w.w.Write([]byte(w.cpfx))
		if err != nil {
			return 0, err
		}
		w.llen = w.cpfxlen
		w.addprefix = false
	}
	var i int
	if !w.linechars && p[0] == ' ' {
		i = 1
		w.llen += nrunes - 1
	} else {
		w.llen += nrunes
	}
	n, err = w.w.Write(p[i:])
	w.linechars = true
	if err == nil {
		n = len(p)
		if p[n-1] == '\n' {
			w.llen = 0
			w.linechars = false
			w.addprefix = true
		}
	}
	return
}
