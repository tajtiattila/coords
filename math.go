// Copyright 2014 Attila Tajti. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// #include <math.h>
import "C"

func sqrtf(f float32) float32  { return float32(C.sqrtf(C.float(f))) }
func floorf(f float32) float32 { return float32(C.floorf(C.float(f))) }
func ceilf(f float32) float32  { return float32(C.ceilf(C.float(f))) }
