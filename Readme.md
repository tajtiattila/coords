
Elite: Dangerous coordinate verifier
====================================

The verifier loads the systems.json file, and checks if the coordinates are correct
based on the 1/32 Ly grid.

The idea is to generate all possible 3D coordinates based on integer arithmeric, that
can yield the on-screen value.

Then then these coordinates are placed on a map. This is done in turn for all reference
systems where the distance is provided. The position of the system being calculated must be
the single grid coordinate left on the map, or one of them, if there are multiple candidates.

A unit test is added that verifies if the algorithm to calculate integer grid coordinates are valid.

Installation
------------

1. Install go
2. clone the project
3. `go build`

Be advised, due to the mapping algorithm is uses a lot of memory. Reduce the number of workers in
coords.go to minimize memory consumption (~1.5G per worker), or to run it on a 32-bit system.
