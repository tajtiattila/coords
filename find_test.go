// Copyright 2014 Attila Tajti. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"math"
	"math/rand"
	"testing"
)

func less(a, b Gridp) bool {
	if a[0] != b[0] {
		return a[0] < b[0]
	}
	if a[1] != b[1] {
		return a[1] < b[1]
	}
	return a[2] < b[2]
}

var testdists = []float64{
	// LDS 1503
	6.35,
	9.23,
	4.06,
	55.658,
	45.232,
	63.903,
	71.188,
	43.027,
	44.641,
	52.075,
	27.504,
	81.155,
	67.981,
	73.612,

	// LHS 3297
	46.021,
	50.041,
	28.801,
	35.571,
	58.022,
	47.161,
	100.201,
	82.84,
	87.061,

	// WREDGUIA WH-Q B46-2
	71.927,
	89.851,
	59.226,
	131.337,
	130.724,
}

func randf() float64 {
	return float64(rand.Intn(100000)) / 1e3
}

// TestGridpDistOrder tests whether DistGridp results are ordered properly
func TestGridpDistOrder(t *testing.T) {
	for i := 0; i < 100; i++ {
		var dist float64
		if i < len(testdists) {
			dist = testdists[i]
		} else {
			dist = randf()
		}
		var last Gridp
		nerr := 0
		DistGridp(dist, func(p Gridp) {
			if !less(last, p) {
				t.Error(last, "is not less than value:", p)
				nerr++
			}
			last = p
			if nerr > 100 {
				t.FailNow()
			}
		})
		if nerr == 0 {
			t.Log(dist, "ok")
		}
	}
}

// TestGridpDistAll tests whether DistGridp lists all possible coords for dists
func TestGridpDistAll(t *testing.T) {
	var vofs []Gridp
	const rng Gridc = 2
	for x := -rng; x <= rng; x++ {
		for y := -rng; y <= rng; y++ {
			for z := -rng; z <= rng; z++ {
				if x != 0 || y != 0 || z != 0 {
					vofs = append(vofs, Gridp{x, y, z})
				}
			}
		}
	}
	for i := 0; i < 100; i++ {
		m := make(map[Gridp]bool)
		var dist float64
		if i < len(testdists) {
			dist = testdists[i]
		} else {
			dist = randf()
		}
		DistGridp(dist, func(p Gridp) {
			m[p] = true
		})
		//sf := screenFloat(float32(dist))
		for p := range m {
			for _, o := range vofs {
				n := Gridp{p[0] + o[0], p[1] + o[1], p[2] + o[2]}
				diff := math.Abs(dist - float64(abs(n)))
				if diff < prec && !m[n] {
					t.Error(n, "is missing from set:", abs(n), "~", dist)
				}
			}
		}
	}
}
